import { Reparation } from "./reparation.model";
import { ResponsableAtelier } from "./responsableAtelier.model";
import { Voiture } from "./voiture.model";

export class Depot{
    private id: string | undefined;
    private voiture : Voiture | undefined;
    private responsable : ResponsableAtelier | undefined;
    private dateDebutReparation : Date | undefined;
    private dateFinReparation : Date | undefined;
    private dateDepot : Date | undefined;
    private dateRecupertion : Date | undefined;
    private reparations : Array<Reparation> = [];
    
    constructor(id : string | undefined){
        this.id = id;
    }

    setId(id : string | undefined ){
        this.id = id;
    }

    getId() : string | undefined{
        return this.id;
    }

    setVoiture(voiture : Voiture | undefined){
        this.voiture = voiture;
    }

    getVoiture() :  Voiture | undefined{
        return this.voiture;
    }

    setResponsable(responsable : ResponsableAtelier | undefined){
        this.responsable = responsable;
    }

    getResponsable() :  ResponsableAtelier | undefined{
        return this.responsable;
    }

    setDateDebutReparation(date : Date | undefined){
        this.dateDebutReparation = date;
    }

    getDateDebutReparation() :  Date | undefined{
        return this.dateDebutReparation;
    }

    setDateFinReparation(date : Date | undefined){
        this.dateFinReparation = date;
    }

    getDateFinReparation() :  Date | undefined{
        return this.dateFinReparation;
    }

    setDateDepot(date : Date | undefined){
        this.dateDepot = date;
    }

    getDateDepot() :  Date | undefined{
        return this.dateDepot;
    }

    setDateRecuperation(date : Date | undefined){
        this.dateRecupertion = date;
    }

    getDateRecuperation() :  Date | undefined{
        return this.dateRecupertion;
    }

    addReparation(rep : Reparation){
        this.reparations.push(rep);
    }

    getReparations() : Array<Reparation>{
        return this.reparations;
    }
}