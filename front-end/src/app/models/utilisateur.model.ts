export class Utilisateur{
    private id: string | undefined | null;
    private nom: string | undefined | null;
    private prenom: string | undefined | null;
    private pseudo: string | undefined | null;
    private email: string | undefined | null;
    private motDePasse: string | undefined | null;
    private role: string | undefined | null;

    /*constructor(email: string | undefined | null, motDePasse: string | undefined | null){
        this.email = email;
        this.motDePasse = motDePasse;
    }
*/
    constructor(id : string | undefined | null,  nom: string | undefined | null,prenom: string | undefined | null,email: string | undefined | null, motDePasse: string | undefined | null,pseudo?: string | undefined | null){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.motDePasse = motDePasse;
        if(pseudo) this.pseudo = pseudo;
    }

    setId(id : string | undefined | null){
        this.id = id;
    }

    getId() : string | undefined | null{
        return this.id;
    }

    setEmail(email : string | undefined | null){
        this.email = email;
    }

    getEmail() : string | undefined | null{
        return this.email;
    }

    setRole(role : string | undefined | null){
        this.role = role;
    }

    getRole() : string | undefined | null{
        return this.role;
    }

    static valueOf(item : {id : string, email : string, role : string}) : Utilisateur {
        let utilisateur : Utilisateur = new Utilisateur(undefined, undefined, undefined, undefined, undefined);
        utilisateur.setId(item.id);
        utilisateur.setEmail(item.email);
        utilisateur.setRole(item.role);
        return utilisateur;
    }
}
