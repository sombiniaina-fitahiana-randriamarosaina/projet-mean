import { ResponsableAtelier } from "./responsableAtelier.model";
import { Voiture } from "./voiture.model";

export class Reparation{
    private motif : string | undefined;
    private montant : string | undefined;
    private estValider : boolean | undefined;
    private estTerminer : boolean | undefined;
    
    constructor(motif : string | undefined, montant : string | undefined, estValider : boolean | undefined, estTerminer : boolean | undefined){
        this.motif = motif;
        this.montant = montant;
        this.estValider = estValider;
        this.estTerminer = estTerminer;
    }


    getMotif() : string | undefined{
        return this.motif;
    }
}