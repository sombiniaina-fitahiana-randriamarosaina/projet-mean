import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { DragDropModule } from '@angular/cdk/drag-drop'

import { AppComponent } from './app.component';
import { PageLoginComponent } from './page-login/page-login.component';
import { UtilisateurService } from './services/utilisateur.service'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { WorkspaceComponent } from './workspace/workspace.component';
import { NotificationHandlerService } from './services/notification.handler.service';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { PageRegisterComponent } from './page-register/page-register.component';
import { RepairHistoryComponent } from './workspace/repair-history/repair-history.component';
import { MyCarsComponent } from './workspace/my-cars/my-cars.component';
import { CarFormComponent } from './workspace/car-form/car-form.component';
import { ClientService } from './services/client.service';
import { RepairsComponent } from './workspace/repairs/repairs.component';
import { DashboardRfComponent } from './workspace/dashboard-rf/dashboard-rf.component';
import { ResponsableAtelierService } from './services/responsableAtelier.service';
import { RepairFormComponent } from './workspace/repair-form/repair-form.component';

const appRoutes : Routes = [
  { path : 'login',     canActivate : [UtilisateurService], component : PageLoginComponent},
  { path : 'register',  canActivate : [UtilisateurService], component : PageRegisterComponent },
  { path : 'workspace', canActivate : [UtilisateurService], component : WorkspaceComponent, children : [
    { path : 'repair-history', component : RepairHistoryComponent },
    { path : 'my-cars', component : MyCarsComponent },
    { path : 'repairs', component : RepairsComponent },
    { path : 'dashboard-rf', component : DashboardRfComponent }
  ]},
  { path : '',          canActivate : [UtilisateurService], component : NotFoundComponent },
  { path : 'not-found', component : NotFoundComponent },
  { path : '**', redirectTo : '/not-found' }
]

@NgModule({
  declarations: [
    AppComponent,
    PageLoginComponent,
    WorkspaceComponent,
    NotFoundComponent,
    PageRegisterComponent,
    RepairHistoryComponent,
    MyCarsComponent,
    CarFormComponent,
    RepairsComponent,
    DashboardRfComponent,
    RepairFormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    DragDropModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    NotificationHandlerService,
    UtilisateurService,
    ClientService,
    ResponsableAtelierService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
