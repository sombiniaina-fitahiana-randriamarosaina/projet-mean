import { Component, OnInit } from '@angular/core';
import { BarController, BarElement, CategoryScale, Chart, LinearScale, PointElement, Title } from 'chart.js';

@Component({
  selector: 'app-dashboard-rf',
  templateUrl: './dashboard-rf.component.html',
  styleUrls: ['./dashboard-rf.component.css']
})
export class DashboardRfComponent implements OnInit {
  chartchiffreAfaireJour : any;
  chartchiffreAffaireMois : any;

  ngOnInit(): void {
    Chart.register(BarController, BarElement, PointElement, LinearScale, Title, CategoryScale);
    this.initChiffreAffaireJour();
    this.initChiffreAffaireMois();
  }

  initChiffreAffaireJour(){
    this.chartchiffreAfaireJour = new Chart("chiffreAfaireJour", {
        type: 'bar',
        data: {
          labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
          datasets: [{
            label: 'Bar Chart',
            data: [65, 59, 80, 81, 56, 55, 40],
            backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(75, 192, 192, 1)']
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
  }


  initChiffreAffaireMois(){
    this.chartchiffreAffaireMois = new Chart("chiffreAffaireMois", {
        type: 'bar',
        data: {
          labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
          datasets: [{
            label: 'Bar Chart',
            data: [65, 59, 80, 81, 56, 55, 40, 45, 98, 20, 15, 15],
            backgroundColor: ['rgba(54, 162, 235)', 'rgb(201, 203, 207)']
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
  }
}
