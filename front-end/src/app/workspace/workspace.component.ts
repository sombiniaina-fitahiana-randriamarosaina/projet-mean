import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnectableObservable, Subscription } from 'rxjs';
import { Utilisateur } from '../models/utilisateur.model';
import { UtilisateurService } from '../services/utilisateur.service';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit, OnDestroy {

  utilisateur : Utilisateur | undefined = undefined;
  infoSubscription : Subscription | undefined = undefined;

  constructor(
    private utilisateurService: UtilisateurService,
    private router : Router
  ) { 
  }

  ngOnInit() {
    this.infoSubscription = this.utilisateurService.utilisateurSubject.subscribe({
      next : (utilisateur : Utilisateur) => {
        this.utilisateur = utilisateur;
      }
    });
    this.utilisateurService.loadUtilisateur();
  }

  ngOnDestroy(): void {
    this.infoSubscription?.unsubscribe();
  }

  logout(event : Event) {
    event.preventDefault()
    this.utilisateurService.removeToken();
    this.router.navigate(['login'])
  }

  isSideBarHide : boolean = false;

  toggleSidebar(){
    this.isSideBarHide = !this.isSideBarHide;
  }

  isSearchBarHide : boolean = false;

  toggleSearchBar(){
    this.isSearchBarHide = !this.isSearchBarHide;
  }
}
