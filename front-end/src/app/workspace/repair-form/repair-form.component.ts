import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Reparation } from 'src/app/models/reparation.model';
import { NotificationHandlerService } from 'src/app/services/notification.handler.service';
import { ResponsableAtelierService } from 'src/app/services/responsableAtelier.service';

@Component({
  selector: 'app-repair-form',
  templateUrl: './repair-form.component.html',
  styleUrls: ['./repair-form.component.css']
})
export class RepairFormComponent {
  @Input() idDepot : any = undefined;
  @Input() btnCloseModal : any = undefined;

  repairForm : FormGroup = this.formBuilder.group({
    motif : ['', Validators.required],
    montant : ['', [Validators.required, Validators.pattern("[1-9][0-9]*")]],
    estValider : [false, Validators.required],
    estTerminer : [false, Validators.required]
  });

  response : any;
  isLoading : boolean = false;

  constructor(
    private responsableAtelierService : ResponsableAtelierService,
    private notificationHandler : NotificationHandlerService,
    private formBuilder : FormBuilder
  ){}

  clean(){
    this.isLoading = false;
    this.response = undefined;
    this.repairForm.reset({
      motif : '',
      montant : '',
      estValider : false,
      estTerminer : false
    });
  }

  onSubmitForm(){
    this.isLoading = true;
    const formValue = this.repairForm.value;
    let reparation = new Reparation(formValue['motif'], formValue['montant'], formValue['estValider'], formValue['estTerminer']);
    this.responsableAtelierService.addReparation(this.idDepot, reparation, {
      next : (response : any) => {
          this.response = response;
          this.isLoading = false;
          if(response.metaData.status == 200){
            if(this.btnCloseModal) this.btnCloseModal.click();
            this.clean();
            this.notificationHandler.raiseSuccess({message : "reparation ajouter"});
          }
      },
      error : (error : any) => {
        if(error.error.metaData){
          this.response = error.error;
        }
        else this.notificationHandler.raireError(error);
        this.isLoading = false;
      },
      complete : () => {}
  });
  }
}
