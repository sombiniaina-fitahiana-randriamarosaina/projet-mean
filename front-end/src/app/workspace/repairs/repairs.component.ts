import { Component, Directive, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Depot } from 'src/app/models/depot.model';
import { Reparation } from 'src/app/models/reparation.model';
import { ResponsableAtelier } from 'src/app/models/responsableAtelier.model';
import { Voiture } from 'src/app/models/voiture.model';
import { NotificationHandlerService } from 'src/app/services/notification.handler.service';
import { ResponsableAtelierService } from 'src/app/services/responsableAtelier.service';

@Component({
  selector: 'app-repairs',
  templateUrl: './repairs.component.html',
  styleUrls: ['./repairs.component.css']
})
export class RepairsComponent implements OnInit, OnDestroy {
  depots : Depot[] = []
  depotsFiltre : Depot[] = [];

  depotSubjectSubscription : Subscription | undefined = undefined;
  idDepotToUpdate : any;
  response : any;
  isLoading : boolean = false;
  statusSelect : any = "En cours"

  constructor(
    private responsableAtelierService : ResponsableAtelierService,
    private notificationHandler : NotificationHandlerService
  ){}

  ngOnInit(): void {
    this.depotSubjectSubscription = this.responsableAtelierService.depotSubject.subscribe({
      next : (depots)=>{
        this.depots = depots;
        this.filterDepot(undefined);
      }
    });
    this.responsableAtelierService.emitDepotsSubject();
  }

  ngOnDestroy(): void {
    this.depotSubjectSubscription?.unsubscribe();
  }

  filterDepot(event : Event | undefined){
    if(this.statusSelect === "En cours"){
      this.depotsFiltre = this.depots.filter(depot => depot.getDateFinReparation() == undefined);
    }
    else if(this.statusSelect === "Terminée"){
      this.depotsFiltre = this.depots.filter(depot => depot.getDateFinReparation() != undefined);
    }
  }

  setIdDepotToUpdate(idDepot : any){
    this.idDepotToUpdate = idDepot;
  }


  finish(idDepot : any){
    this.responsableAtelierService.finishDepot(idDepot, {
      next : (response : any) => {
        this.response = response;
        this.isLoading = false;
        if(response.metaData.status == 200){
          this.notificationHandler.raiseSuccess({message : "Reparation Terminee"});
        }
    },
    error : (error : any) => {
      if(error.error.metaData){
        this.response = error.error;
      }
      else this.notificationHandler.raireError(error);
      this.isLoading = false;
    },
    complete : () => {}
    })
  }
}
