import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Client } from 'src/app/models/client.model';
import { Utilisateur } from 'src/app/models/utilisateur.model';
import { Voiture } from 'src/app/models/voiture.model';
import { ClientService } from 'src/app/services/client.service';
import { NotificationHandlerService } from 'src/app/services/notification.handler.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-my-cars',
  templateUrl: './my-cars.component.html',
  styleUrls: ['./my-cars.component.css']
})
export class MyCarsComponent implements OnInit, OnDestroy {
  listeVoiture : Voiture[] = []
  listeVoitureDepose : Voiture[] = []

  utilisateurSubscription : Subscription | undefined = undefined;
  voituresSubscription : Subscription | undefined = undefined;

  isLoading : boolean = false;

  constructor(
    private utilisateurService : UtilisateurService,
    private clientService : ClientService,
    private notificationHandler : NotificationHandlerService,
  ){}

  ngOnInit(): void {
    this.voituresSubscription = this.clientService.voituresSubject.subscribe({
      next : (voitures)=>{
        let listeVoiture : Voiture[] = [];
        let listeVoitureDepose : Voiture[] = [];
        voitures.forEach(voiture => {
          if(voiture.getIdReparation() != undefined) listeVoitureDepose.push(voiture);
          else listeVoiture.push(voiture)
        });
        this.listeVoiture = listeVoiture;
        this.listeVoitureDepose = listeVoitureDepose;
      }
    });
    this.utilisateurSubscription = this.utilisateurService.utilisateurSubject.subscribe({
      next : (utilisateur : Utilisateur) => {
        this.clientService.loadVoitures();
      }
    });

    if(this.utilisateurService.getUtilisateur().getId() != undefined){
      this.clientService.loadVoitures();
    }
  }

  ngOnDestroy(): void {
    this.voituresSubscription?.unsubscribe();
    this.utilisateurSubscription?.unsubscribe();
  }

  edit(id : string | undefined | null){
    console.log(id);
  }

  remove(id : string | undefined | null){
    this.isLoading = true;
    let observale = this.clientService.removeVoiture(id, {
      next : (response : any) => {
        if(response.metaData.status == 200){
          this.notificationHandler.raiseSuccess({message : "Voiture supprimée"});
        }
        else{
          this.notificationHandler.raireError(response);
        }
    },
    error : (error : any) => {
      this.notificationHandler.raireError(error);
    },
    complete : () => {
      this.isLoading = false;
    }
    })
  }

  depose(id : string | undefined | null){
    this.isLoading = true;
    this.clientService.deposeVoiture(id, {
      next : (response : any) => {
        if(response.metaData.status == 200){
          this.notificationHandler.raiseSuccess({message : "Voiture deposée"});
        }
        else{
          this.notificationHandler.raireError(response);
        }
    },
    error : (error : any) => {
      this.notificationHandler.raireError(error);
    },
    complete : () => {
      this.isLoading = false;
    }
    })
  }

  onDropListeVoiture(event: CdkDragDrop<string[]>) {
    this.clientService.deplacerVoiture(event.previousIndex, event.currentIndex)
  }

  onDrop(event: CdkDragDrop<Voiture[], any, any>) {
    this.clientService.deplacerVoiture(event.previousIndex, event.currentIndex)
  }
}
