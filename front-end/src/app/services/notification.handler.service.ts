import { Subject } from "rxjs";


export const USER_ALREADY_LOGGED = "Vous etes déjà connecté!!"
export const USER_NOT_LOGGED = "Veuillez vous connecter svp!!"
export const CONNECTON_LOST = "Vous etes hors connexion"


export class NotificationHandlerService {
    errorSubject = new Subject<any>();
    successSubject = new Subject<any>();

    constructor(){}

    raireError(error : any){
        this.errorSubject.next(error.message);
    }

    raiseSuccess(success : any){
        this.successSubject.next(success.message);
    }
}
