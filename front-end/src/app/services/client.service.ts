import { moveItemInArray } from "@angular/cdk/drag-drop";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { observable, Observable, Subject } from "rxjs";
import { Client } from "../models/client.model";
import { Utilisateur } from "../models/utilisateur.model";
import { Voiture } from "../models/voiture.model";
import { environment } from './../../environments/environment';
import { NotificationHandlerService } from "./notification.handler.service";
import { UtilisateurService } from "./utilisateur.service";

@Injectable()
export class ClientService {
	private voitures : Voiture[] = [];

	// subject 
	voituresSubject : Subject<Voiture[]> = new Subject<Voiture[]>();

	constructor(
		private httpclient : HttpClient,
		private notificationHandler : NotificationHandlerService,
		private utilisateurService : UtilisateurService
	) { }

	emitVoituresSubject(){
		this.voituresSubject.next(this.voitures.slice()); 
	}

	setVoitures(voitures : Voiture[]){
		this.voitures = voitures;
		this.emitVoituresSubject();
	}



	loadVoitures(){
		let client = this.utilisateurService.getUtilisateur();
		// console.log(client);
		let token = this.utilisateurService.getToken();
		let httpHeaders : HttpHeaders = new HttpHeaders().append('Authorization', `Bearer ${token}`);
		let observable = this.httpclient.get(`${environment.apiUrl}/api/client/${client.getId()}/voitures`, {headers : httpHeaders});
		let subscription = observable.subscribe({
			next : (response : any) => {
				if(response.metaData.status == 200){
				  this.setVoitures(Voiture.valuesOf(response.data.voitures));
				}
			},
			error : (error : any) => {
			  this.notificationHandler.raireError(error);
			},
			complete : () => {
			  subscription.unsubscribe();
			}
		})
		
	}

	deplacerVoiture(previousIndex : number, currentIndex : number){
		moveItemInArray(this.voitures, previousIndex, currentIndex);
		this.emitVoituresSubject();
	}

	addVoiture(voiture : Voiture, toDo : { next : any , error : any, complete : any}) : Observable<any>{
		let client = this.utilisateurService.getUtilisateur();
		let token = this.utilisateurService.getToken();

		let httpHeaders : HttpHeaders = new HttpHeaders().append('Authorization', `Bearer ${token}`);

		const observale = this.httpclient.post(`${environment.apiUrl}/api/client/${client.getId()}/voitures`, voiture,{headers : httpHeaders});

		const subscription = observale.subscribe({
			next : (response : any) => {
				toDo.next(response);
				if(response.metaData.status == 200) {
					voiture.setNew(true);
					voiture.setId(response.data.id);
					this.voitures.unshift(voiture);
					this.emitVoituresSubject();	
				}
			},
			error : (error) => {
				toDo.error(error);
			},
			complete : () => {
				toDo.complete();
				subscription.unsubscribe();
			}
		})
		return observale;
	}

	removeVoiture(idVoiture : string | undefined | null, toDo : { next : any , error : any, complete : any}) : Observable<any>{
		let client = this.utilisateurService.getUtilisateur();
		let token = this.utilisateurService.getToken();
		
		let httpHeaders : HttpHeaders = new HttpHeaders().append('Authorization', `Bearer ${token}`);

		const observale = this.httpclient.delete(`${environment.apiUrl}/api/client/${client.getId()}/voitures/${idVoiture}`, {headers : httpHeaders});

		const subscription = observale.subscribe({
			next : (response : any) => {
				toDo.next(response);
				if(response.metaData.status == 200) {
					this.voitures = this.voitures.filter(function( voiture ) {
						return voiture.getId() !== idVoiture;
					});
					this.emitVoituresSubject();	
				}
			},
			error : (error) => {
				toDo.error(error);
			},
			complete : () => {
				toDo.complete();
				subscription.unsubscribe();
			}
		})
		return observale;
	}


	deposeVoiture(idVoiture : string | undefined | null, toDo : { next : any , error : any, complete : any}) : Observable<any>{
		let client = this.utilisateurService.getUtilisateur();
		let token = this.utilisateurService.getToken();

		let httpHeaders : HttpHeaders = new HttpHeaders().append('Authorization', `Bearer ${token}`);

		const observale = this.httpclient.put(`${environment.apiUrl}/api/client/${client.getId()}/voitures/${idVoiture}/deposer`, {}, {headers : httpHeaders});

		const subscription = observale.subscribe({
			next : (response : any) => {
				toDo.next(response);
				if(response.metaData.status == 200) {
					const {id , marque, matricule, idReparation} = response.data;

					var foundIndex = this.voitures.findIndex(x => x.getId() == id);

					this.voitures[foundIndex].setMarque(marque);
					this.voitures[foundIndex].setMatricule(matricule);
					this.voitures[foundIndex].setIdReparation(idReparation);

					this.emitVoituresSubject();	
				}
			},
			error : (error) => {
				toDo.error(error);
			},
			complete : () => {
				toDo.complete();
				subscription.unsubscribe();
			}
		})
		return observale;
	}

	// Fonction temporaire akaa idFotsiny
	makeid(length : number) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+{}[]:;<>,./?';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}
}
