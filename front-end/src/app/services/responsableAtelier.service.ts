import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs'
import { Depot } from '../models/depot.model'
import { Reparation } from '../models/reparation.model'
import { ResponsableAtelier } from '../models/responsableAtelier.model'
import { Voiture } from '../models/voiture.model'
import { NotificationHandlerService } from './notification.handler.service'

@Injectable()
export class ResponsableAtelierService {
  private depots: Depot[] = []

  // subject
  depotSubject: Subject<Depot[]> = new Subject<Depot[]>()

  constructor (
    private httpclient: HttpClient,
    private notificationHandler: NotificationHandlerService
  ) {
    let RA1 = new ResponsableAtelier('responsableAtelier1', 'Responsable', 'Atelier', 'responsable1.atelier@mean.com', '123456', 'RA')
    let RA2 = new ResponsableAtelier( 'responsableAtelier2', 'Responsable', 'Atelier', 'responsable2.atelier@mean.com', '123456', 'RA')

    let voiture1 = new Voiture('voiture1', 'BMW', '1542TAF')
    let voiture2 = new Voiture('voiture2', 'TOYOTA RAV4', '5468TBG')

    let reparation1 = new Reparation('Frein avant', '100', false, false)
    let reparation2 = new Reparation('Filtre', '100', false, false)
    let reparation3 = new Reparation("Plateau d'embrayage", '100', false, false)

    let depot1 = new Depot('depot1')
    depot1.setVoiture(voiture1)
    depot1.setResponsable(RA1)
    depot1.setDateDepot(new Date())
    depot1.addReparation(reparation1)
    depot1.addReparation(reparation2)

    let depot2 = new Depot('depot2')
    depot2.setVoiture(voiture2)
    depot2.setResponsable(RA2)
    depot2.setDateDepot(new Date())
    depot2.addReparation(reparation3)

    this.depots.push(depot1)
    this.depots.push(depot2)
  }

  emitDepotsSubject () {
    this.depotSubject.next(this.depots.slice())
  }

  addReparation ( idDepot: string, reparation: Reparation, toDo: { next: any; error: any; complete: any }) {
    const observale = new Observable(subscriber => {
      setTimeout(() => {
        subscriber.next({
          metaData: {
            status: 200,
            message: 'ok'
          }
        })
        subscriber.complete()
      }, 1000)
    })

    const subscription = observale.subscribe({
      next: (response: any) => {
        toDo.next(response)
        if (response.metaData.status == 200) {
          const depot = this.depots.find(depot => depot.getId() == idDepot)
          depot?.addReparation(reparation)
          this.emitDepotsSubject()
        }
      },
      error: error => {
        toDo.error(error)
      },
      complete: () => {
        toDo.complete()
        subscription.unsubscribe()
      }
    })
  }

  finishDepot ( idDepot: string, toDo: { next: any; error: any; complete: any }) {
    const observale = new Observable(subscriber => {
      setTimeout(() => {
        subscriber.next({
          metaData: {
            status: 200,
            message: 'ok'
          }
        })
        subscriber.complete()
      }, 1000)
    })

    const subscription = observale.subscribe({
      next: (response: any) => {
        toDo.next(response)
        if (response.metaData.status == 200) {
          const depot = this.depots.find(depot => depot.getId() == idDepot)
          depot?.setDateFinReparation(new Date());
          this.emitDepotsSubject()
        }
      },
      error: error => {
        toDo.error(error)
      },
      complete: () => {
        toDo.complete()
        subscription.unsubscribe()
      }
    })
  }

}
