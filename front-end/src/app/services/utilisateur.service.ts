import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, TitleStrategy, UrlTree } from '@angular/router';
import { Observable, Subject } from 'rxjs';

import { Utilisateur } from '../models/utilisateur.model';
import { Voiture } from '../models/voiture.model';
import { environment } from '../../environments/environment';
import { ClientService } from './client.service';
import { NotificationHandlerService, USER_ALREADY_LOGGED, USER_NOT_LOGGED } from './notification.handler.service';

const TOKEN_VALUE : string = "token-value";

@Injectable()
export class UtilisateurService implements CanActivate{
    private utilisateur : Utilisateur = new Utilisateur(undefined, undefined, undefined, undefined, undefined, undefined);

    // subject 
    utilisateurSubject : Subject<Utilisateur> = new Subject<Utilisateur>();

    constructor(
        private httpclient : HttpClient,
        private router : Router,
        private notificationHandler : NotificationHandlerService,
    ){}

    emitUtilisateurSubject(){
		this.utilisateurSubject.next(this.utilisateur); 
	}

	setUtilisateur(utilisateur : Utilisateur){
        this.utilisateur = utilisateur;
		this.emitUtilisateurSubject();
	}

    // Http
    loadUtilisateur(){
        let token = this.getToken();
		let httpHeaders : HttpHeaders = new HttpHeaders().append('Authorization', `Bearer ${token}`);
		let observable = this.httpclient.get(environment.apiUrl + "/api/utilisateur/info", {headers : httpHeaders});
		let subscription = observable.subscribe({
			next : (response : any) => {
				if(response.metaData.status == 200){
				  this.setUtilisateur(Utilisateur.valueOf(response.data));
				}
			},
			error : (error : any) => {
			  this.notificationHandler.raireError(error);
			},
			complete : () => {
			  subscription.unsubscribe();
			}
		})
    }

    auth(utilisateur: Utilisateur) : Observable<any>{
        return this.httpclient.post(environment.apiUrl + "/api/utilisateur/connexion", utilisateur);
    }

    signUp(utilisateur: Utilisateur) : Observable<any>{
        return this.httpclient.post(environment.apiUrl + "/api/utilisateur/inscription", utilisateur);
    }

    // Other Method
    setToken(data : {token : string}){
        localStorage.clear();
        localStorage.setItem(TOKEN_VALUE, data.token);
        // localStorage.setItem(TOKEN_EXPIRARTION_DATE, data.token.expiration);
    }

    isAuth(){
        return (localStorage.getItem(TOKEN_VALUE) != null);
    }

    getToken(){
        return localStorage.getItem(TOKEN_VALUE);
    }

    removeToken(){
        localStorage.clear();
    }

    getUtilisateur() : Utilisateur{
        return this.utilisateur;
    }

    // Guard
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        // quand l'utilisateur entre dans "/"
        if(route.url.length == 0){
            if(this.isAuth()) this.goToWorkspace(false);
            else this.goToLogin(false);
        }
        else{
            // Quand l'utilisateur veut consulter la page d'auth ou d'inscription alors qu'il est déja connecté.
            if((route.url[0].path == 'login' || route.url[0].path == 'register') && this.isAuth() ){
                this.goToWorkspace(true);
            }
            // Quand l'utilisateur veut consulter autre page alors qu'il n'est pas connecté.
            else if((route.url[0].path != 'login' && route.url[0].path != 'register') && !this.isAuth()){
                this.goToLogin(true);
            }
        }
        return true;
    }

    private goToLogin(raiseError : boolean){
        if(raiseError) this.notificationHandler.raireError({message : USER_NOT_LOGGED});
        this.router.navigate(['/login']);
    }

    private goToWorkspace(raiseError : boolean){
        if(raiseError) this.notificationHandler.raireError({message : USER_ALREADY_LOGGED});
        this.router.navigate(['/workspace']);
    }
}
