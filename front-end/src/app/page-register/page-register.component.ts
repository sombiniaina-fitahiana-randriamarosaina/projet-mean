import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilisateurService } from '../services/utilisateur.service';
import { Utilisateur } from '../models/utilisateur.model';
import { NotificationHandlerService } from '../services/notification.handler.service';


@Component({
  selector: 'app-page-register',
  templateUrl: './page-register.component.html',
  styleUrls: ['./page-register.component.css']
})
export class PageRegisterComponent implements OnInit{
    utilisateurForm : FormGroup = this.formBuilder.group({
      pseudo: '',
      email : '',
      motDePasse : ''
    });
  response : any;
  isLoading : boolean = false;
  isSubscribe : boolean = false;

  constructor(
    private notificationHandler : NotificationHandlerService,
    private formBuilder : FormBuilder,
    private utilisateurService : UtilisateurService,
    private router : Router
  ){}

  ngOnInit(): void {}

  onSubmitForm(){
    this.isLoading = true;
    const formValue = this.utilisateurForm.value;
    let utilisateur = new Utilisateur(undefined, formValue['nom'], formValue['prenom'], formValue['email'], formValue['motDePasse'], formValue['pseudo']);
    let observale = this.utilisateurService.signUp(utilisateur);
    let subscription = observale.subscribe({
        next : (response) => {
          setTimeout( () => {
            this.response = response;
            this.isLoading = false;
            if(response.metaData.status == 200){
              // this.utilisateurService.setData(response.data)this.
              this.router.navigate(["login"]);
            }
          }, 3000)
        },
        error : (error) => {
          if(error.error.metaData){
            this.response = error.error;
          }
          else this.notificationHandler.raireError(error);
          this.isLoading = false;
        },
        complete : () => {
          subscription.unsubscribe();
        }
    })
  }

  goToLoginPage(e : Event){
    e.preventDefault();
    this.router.navigate(["login"]);
  }
}
