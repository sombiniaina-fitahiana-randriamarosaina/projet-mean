const connect = async (success) => {
    const mongoose = require('mongoose');
    mongoose.set('strictQuery', false)
    //connection
    mongoose.connect(process.env.DATABASE_CONNECTION_STRING)

    const session = await mongoose.startSession();
    // to do
    await success(mongoose);
    // mongoose.disconnect()
}

const existModel = (mongoose, modelName) => {
    return mongoose.modelNames().includes(modelName);
}

const getModel =  (mongoose, collectionName, schema) => {
    return existModel(mongoose, collectionName) ? mongoose.model(collectionName) : mongoose.model(collectionName, schema);
}


exports.getModel = getModel
exports.connect = connect