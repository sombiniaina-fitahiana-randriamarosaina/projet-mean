const IsEmail = require('isemail');

class ErrorFieldControl{
    constructor(){
        this.errors = {};
    }

    addError(key, message){
        if(!(key in this.errors)) this.errors[key] = message
    }

    haveErrors(){
        return Object.keys(this.errors).length != 0;
    }

    getErrors(){
        return this.errors;
    }

    required(key, value){
        if(value?.trim() === "" || value == undefined || value == null) this.addError(key, `Merci de remplire le champ ${key}`)
    }

    email(key, value){
        if(!IsEmail.validate(value??"")) this.addError(key, `Merci de renseigner le champ ${key} avec un format d'email valide`)
    }
    
}

module.exports = ErrorFieldControl;