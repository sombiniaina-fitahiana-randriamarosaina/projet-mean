const express = require('express');
const router = express.Router();
const clientRequestHandler = require('../RequestHandlers/ClientRequestHandler');
const utilisateurRequestHandler = require('../RequestHandlers/UtilisateurRequestHandler');

router.use(express.json()); //middleware json integre
router.use(utilisateurRequestHandler.controleToken); //middleware control Token
router.param('idClient', clientRequestHandler.controlParamIdClient); //middleware control params idUtilisateur

// Route
router.post('/:idClient/voitures', clientRequestHandler.ajoutVoiture);
router.get('/:idClient/voitures', clientRequestHandler.listerVoiture);
router.delete('/:idClient/voitures/:idVoiture', clientRequestHandler.deleteVoiture);
router.put('/:idClient/voitures/:idVoiture/deposer', clientRequestHandler.deposerVoiture);

module.exports = router;