const utils = require('../Tools/utils');
const bcrypt = require("bcrypt"); // Crypter mot de passe
const { getModel } = require('../Tools/Connection');
const ErrorFieldControl = require('../Tools/ErrorFieldControl');

class Utilisateur {
    mongo = {
        collectionName: "utilisateurs",
        schema: {
            nom :{ type: String },
            prenom :{ type: String },
            pseudo :{ type: String },
            email: { type: String },
            motDePasse: { type: String },
            role: { type: String }, // admin, client, responsable atelier, responsable fianciere
            token: { type: String },
            voitures : { type : Array },
            idReparation : {type : String}
        }
    }

    constructor(id, nom, prenom, pseudo, email, motDePasse, role) {
        this._id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.email = email;
        this.motDePasse = motDePasse;
        this.role = role;
        this.token = undefined;
    }

    getId(){
        return this._id;
    }

    setToken(token){
        this.token = token;
    }

    async getIdByToken(mongoose) {
        let success = true;
        let errors = undefined;
        let data = undefined;

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        const filtre = { token: this.token }
        await utilisateur.find(filtre).then(async (doc) => {
            if (doc.length == 0) {
                success = false;
                errors = { '_global_': "Token invalide ou expire" }
            }
            else {
                let utilisateurDB = doc[0];
                data = {
                    id : utilisateurDB._id
                }
            }
        })
        return { success: success, errors: errors, data: data };
    }

    controle(){
        let errorFieldControl = new ErrorFieldControl();
        errorFieldControl.required('email', this.email);
        errorFieldControl.required('motDePasse', this.motDePasse)
        errorFieldControl.email('email', this.email)
        return errorFieldControl;
    }

    async inscription(mongoose) {
        let success = true;
        let data = {};  
        let errors = {};

        let errorFieldControl = this.controle();
        if(errorFieldControl.haveErrors()){
            success = false;
            errors = errorFieldControl.getErrors();
        }

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        const filtre = { 
            $or : [ 
                { $and : [{email: { $regex : this.email, $options: 'i' }}, {email : { $ne: undefined }}] }, 
                { $and : [{pseudo: { $regex : this.pseudo??"", $options: 'i' }}, {pseudo : { $ne: undefined }}] }, 
            ]
        };

        await utilisateur.find(filtre, {email : 1, pseudo : 1}).then(async (doc) => {
            if (doc.length == 0) {
                success = true;
                data = {
                    nom: this.nom,
                    prenom: this.prenom,
                    pseudo: this.pseudo,
                    email: this.email,    
                    motDePasse: bcrypt.hashSync(this.motDePasse, 8),
                    role: "client"
                }
                var user = new utilisateur(data);
                await user.save(); //Insert data
            }
            else {
                success = false;
                doc.forEach(utilisateur => {
                    if(utilisateur.email.toLowerCase() == this.email.toLowerCase()) errors.email = "L'email est deja pris"
                    if(utilisateur.pseudo !== undefined && utilisateur.pseudo.toLowerCase() == this.pseudo.toLowerCase()) errors.pseudo = "Le pseudo est deja pris"
                });
            }
        })
        
        return { success, errors, data }
    }

    async connexion(mongoose) {
        let success = true;
        let errors = undefined;
        let data = undefined;

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        const filtre = { email: this.email }
        await utilisateur.find(filtre).then(async (doc) => {
            if (doc.length == 0) {
                success = false;
                errors = { 'email': "Aucun compte n'est associer a ce mail" }
            }
            else {
                let utilisateurDB = doc[0];
                var verifyPassword = bcrypt.compareSync(this.motDePasse, utilisateurDB.motDePasse); //Comparer mot de passe (Boolean) 
                if (!verifyPassword) {
                    success = false;
                    errors = { '_global_': "Email ou mot de passe incorect!" }
                }
                else {
                    let token = utilisateurDB.token;
                    if(utilisateurDB.token == undefined){
                        token = utils.token();
                        await utilisateur.update({ _id : utilisateurDB._id}, {token : token});
                    }
                    data = {
                        token: {
                            value: token
                        }
                    }
                }
            }
        })
        return { success: success, errors: errors, data: data };
    }

    async findById(mongoose) {
        let success = true;
        let errors = undefined;
        let data = undefined;

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        const filtre = { _id: this.getId() }
        await utilisateur.find(filtre).then(async (doc) => {
            if (doc.length == 0) {
                success = false;
                errors = { '_global_': "Utilisateur not found" }
            }
            else {
                let utilisateurDB = doc[0];
                data = {
                    id : utilisateurDB.id,
                    email : utilisateurDB.email,
                    role : utilisateurDB.role
                }
            }
        })
        return { success: success, errors: errors, data: data };
    }

}
module.exports = Utilisateur;