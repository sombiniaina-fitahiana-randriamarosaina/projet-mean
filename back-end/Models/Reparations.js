const { getModel } = require("../Tools/Connection");

class Reparation{
    mongo = {
        collectionName: "reparations",
        schema: {
            idVoiture :{ type: String }
        }
    }

    constructor(id, idVoiture) {
        this._id = id;
        this.idVoiture = idVoiture
    }

    getId(){
        return this._id;
    }

    getIdVoiture(){
        return this.idVoiture;
    }

    async save(mongoose) {
        let success = true;
        let errors = undefined;
        let data = {};

        const reparation = await getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        data.idVoiture = this.getIdVoiture();
        let newRep = new reparation(data);
    
        await newRep.save()

        data.id = newRep._id;

        return { success: success, errors: errors, data: data };
    }
}

module.exports = Reparation;