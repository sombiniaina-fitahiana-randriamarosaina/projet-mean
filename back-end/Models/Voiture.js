let ErrorFieldControl = require('../Tools/ErrorFieldControl')

class Voiture {
    constructor(id, marque, matricule){
        this.id = id;
        this.marque = marque?.toUpperCase();
        this.matricule = matricule?.toUpperCase();
    }

    controle(){
        let errorFieldControl = new ErrorFieldControl();
        errorFieldControl.required('marque', this.marque);
        errorFieldControl.required('matricule', this.matricule)
        return errorFieldControl;
    }

    setId(id){
        this.id = id;
    }

    getId(){
        return this.id;
    }
}

module.exports = Voiture;