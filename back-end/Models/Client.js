const Utilisateur = require("./Utilsateur");
const utils = require('../Tools/utils');
const Reparation = require("./Reparations");
const { getModel } = require("../Tools/Connection");

class Client extends Utilisateur{
    async getVoitures(mongoose) {
        let success = true;
        let errors = undefined;
        let data = [];

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        const filtre = {
            _id : this.getId(), 
        }
        await utilisateur.findOne(filtre).then(async (doc) => {
            if (doc.length == 0) {
                success = false;
                errors = { '_global_': "Client inexistant" }
            }
            else {
                data = {
                    voitures : doc.voitures
                }
            }
        })

        return { success: success, errors: errors, data: data };
    }

    async addVoiture(mongoose, voiture) {
        let success = true;
        let errors = undefined;
        let data = [];

        let errorFieldControl = voiture.controle();
        if(errorFieldControl.haveErrors()){
            success = false;
            errors = errorFieldControl.getErrors();
        }else{
            voiture.setId(new mongoose.Types.ObjectId());
            const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
            const filtre = {
                _id : this.getId(), 
                "voitures.matricule" : voiture.matricule
            }
            await utilisateur.find(filtre).then(async (doc) => {
                if (doc.length != 0) {
                    success = false;
                    errors = { 'matricule': "Vous avez deja une voiture avec ce matricule" }
                }
                else {
                    await utilisateur.update(
                        { _id : this.getId()},
                        { $push : {voitures : voiture}}
                    )
                    data = {
                        id : voiture.id,
                        marque: voiture.marque,
                        matricule: voiture.matricule
                    }
                }
            })
        }

        return { success: success, errors: errors, data: data };
    }

    async deleteVoiture(mongoose, voiture) {
        let success = true;
        let errors = undefined;
        let data = [];

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);
        let result = await utilisateur.updateOne(
            { _id : mongoose.Types.ObjectId(this.getId())},
            { $pull : {voitures: {id : mongoose.Types.ObjectId(voiture.getId())}}}
        )
        if(result.modifiedCount != 0){
            data = {
                id : voiture.id,
                marque: voiture.marque,
                matricule: voiture.matricule
            }
        }
        else{
            success  = false;
            errors = {_global_ : "Voiture inexistante"}
        }
        return { success: success, errors: errors, data: data };
    }

    async deposerVoiture(mongoose, voiture) {
        let success = true;
        let errors = undefined;
        let data = {};

        const utilisateur = getModel(mongoose, this.mongo.collectionName, this.mongo.schema);

        const filtre = { _id: mongoose.Types.ObjectId(this.getId()), "voitures.id" : mongoose.Types.ObjectId(voiture.getId())}
        let doc = await utilisateur.find(filtre, { voitures: { $elemMatch: { id: mongoose.Types.ObjectId(voiture.getId()) } } }).then(async (doc) => {
            return doc;
        })
        if(doc.length == 0){
            success = false;
            errors = { '_global_': "l' id du client est incorrect ou l'id du voiture n'existe pas" }
        }
        else if (doc.length != 0 && doc[0].voitures[0].idReparation != undefined) {
            success = false;
            errors = { '_global_': "Votre voiture est deja en cours de reparation" }
        }
        else{
            let reparation = new Reparation(null, mongoose.Types.ObjectId(voiture.getId()))
        
            let ret = await reparation.save(mongoose);

            await utilisateur.updateOne(
                filtre,
                { $set: { "voitures.$.idReparation" : ret.data.id } }
            )
            let voitureDB = doc[0].voitures[0];
            data.id = voitureDB.id;
            data.marque = voitureDB.marque;
            data.matricule = voitureDB.matricule;
            data.idReparation = ret.data.id;
        }

        return { success, errors, data };
    }
}

module.exports = Client;