const Utilisateur = require('../Models/Utilsateur');
const HttpResponse = require('../Tools/HttpResponse');
const connection = require('../Tools/Connection');

exports.controleToken = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    const {token} = request;
    try {
        if(token == undefined){
            httpResponse.setStatus(403);
            httpResponse.setErrors({"token" : "Token absent"});
            response.status(httpResponse.getStatus());
            response.send(httpResponse); 
        }else {
            await connection.connect(async(mongoose) => {
                let utilisateur = new Utilisateur(null, null, null, null, null, null, null);
                utilisateur.setToken(token);
                let objConnexion = await utilisateur.getIdByToken(mongoose);
    
                if(objConnexion.success){
                    request.idUtilisateur = objConnexion.data.id;
                    next();
                }
                else{
                    httpResponse.setStatus(400);
                    httpResponse.setErrors(objConnexion.errors);
                    response.status(httpResponse.getStatus());
                    response.send(httpResponse);    
                }
            }) 
        }
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
}

// INSCRIPTION
exports.inscription = async (request, response, next) => {  
    let httpResponse = new HttpResponse();
    try{
        const {nom, prenom, pseudo, email, motDePasse, role} = request.body;
        let utilisateur = new Utilisateur(undefined, nom, prenom, pseudo, email, motDePasse, role)
        await connection.connect(async (mongoose) => {
            let objConnexion = await utilisateur.inscription(mongoose);

            if(objConnexion.success){
                httpResponse.setStatus(200);
                httpResponse.setData(objConnexion.data);
                httpResponse.statusMessage = "Inscription réussie";
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(objConnexion.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    }catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};
// CONNEXION
exports.connexion = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    const {nom, prenom, pseudo, email, motDePasse, role} = request.body;
    try {
        let utilisateur = new Utilisateur(null, nom, prenom, pseudo, email, motDePasse, role)
        const mongoose = await connection.connect(async (mongoose) => {
            let objConnexion = await utilisateur.connexion(mongoose);

            if(objConnexion.success){
                httpResponse.setStatus(200);
                httpResponse.setData(objConnexion.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(objConnexion.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};

// INFO
exports.info = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        let utilisateur = new Utilisateur(request.idUtilisateur, null, null, null, null, null, null);
        const mongoose = await connection.connect(async (mongoose) => {
            let objConnexion = await utilisateur.findById(mongoose);

            if(objConnexion.success){
                httpResponse.setStatus(200);
                httpResponse.setData(objConnexion.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(objConnexion.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};