const Client = require('../Models/Client');
const HttpResponse = require('../Tools/HttpResponse');
const connection = require('../Tools/Connection');
const Voiture = require('../Models/Voiture');

// Conrole Idlient
exports.controlParamIdClient = (request, response, next) => {
    /* le id est renseigner */
    if(request.params.idClient == undefined || request.params.idClient == null || request.params.idClient == ""){
        let httpResponse = new HttpResponse();
        httpResponse.setStatus(400);
        httpResponse.setErrors({_global_ : "veuillez renseigner le parametre idClient"});

        response.status(httpResponse.getStatus());
        response.send(httpResponse);  
    }
    else{
        next();
    }
}

// getListeVoiture
/* 
    Params : 
        - idClient by request.params
*/
exports.listerVoiture = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        await connection.connect(async (mongoose) => {
            let client = new Client(request.params.idClient);
            let obj = await client.getVoitures(mongoose);

            if(obj.success){
                httpResponse.setStatus(200);
                httpResponse.setData(obj.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(obj.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};

// Ajout Voiture
/* 
    Params : 
        - idClient by request.params
        - marque by request.body
        - matricule by request.body
*/
exports.ajoutVoiture = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        await connection.connect(async (mongoose) => {
            let {marque, matricule} = request.body;
            let voiture = new Voiture(null, marque, matricule);
            let client = new Client(request.params.idClient);
            let obj = await client.addVoiture(mongoose, voiture);

            if(obj.success){
                httpResponse.setStatus(200);
                httpResponse.setData(obj.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(obj.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};


// Delete Voiture
/* 
    Params : 
        - idClient by request.params
        - idVoiture by request.params
*/
exports.deleteVoiture = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        await connection.connect(async (mongoose) => {
            let {marque, matricule} = request.body;
            let voiture = new Voiture(request.params.idVoiture, marque, matricule);
            let client = new Client(request.params.idClient);
            let obj = await client.deleteVoiture(mongoose, voiture);

            if(obj.success){
                httpResponse.setStatus(200);
                httpResponse.setData(obj.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(obj.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};

// deposer Voiture
/* 
    Params : 
        - idClient by request.params
        - idVoiture by request.params
*/
exports.deposerVoiture = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        await connection.connect(async (mongoose) => {
            let {marque, matricule} = request.body;
            let voiture = new Voiture(request.params.idVoiture, marque, matricule);
            let client = new Client(request.params.idClient);
            let obj = await client.deposerVoiture(mongoose, voiture);

            if(obj.success){
                httpResponse.setStatus(200);
                httpResponse.setData(obj.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(obj.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};